#### Struktura/Scénář:
- Aplikace
	- Udržuje seznam předmětů, za které je ústav zodpovědný
	- Generování přednášek/seminářů/cvičení podle počtu zapsaných studentů
	- Přiřazení karet předmětů/úvazků jednotlivým zaměstancům (možnost ručně měnit)
	- Pohled: pouze tajemník (bez zabezpečení, jediný uživatel), front end formuláře
	- Kroky
		- Studijní obor/plány (+základ předmětů)
		- Přidání zaměstanců (automatické generovaní)
		- Přidání studentů: ročník, způsob studia, počet, semestr
		
- Studijní plány
	- Předmět - název/zkratka, vyučující, počet přednášek/seminářů/cvičení, způsob ukončení, kredity, semestr, počet osob na cvičení
	- Přidání předmětu, smazání, úprava
- Zaměstanci ústavu
	- Zaměstanec - jméno, telefon/mobil, email, kancelář, role (profesor, docent, odborný asistent, asistent), dohoda/úvazek
	- Úvazek - nejvyšší 1.0, nižší klesají
	- Úvazkový list
		Pro jednotlivé zaměstnance
		Části - A pedagogická činnost (přímá výuka, zkoušení, ostatní), B tvůrčí činnost, C administrativní činnost, D další činnost
			Přímá výuka - předmět, týdny (ZS/LS), rozvrhové hodiny týdně (přednášky, cvičení, semináře, r?), skupiny (přednášky, cvičení, semináře, r?, pracovní body celkem (přednášky, cvičení, semináře, r?
			Zkoušení - předmět, počet studentů (kl, zap, zk), pracovní body celkem (kl, zap, zk)
	- Počty studentů
		Obor(zaměření), typ studia (Bc./Mgr.), ročník, počet zapsaných studentů, rozdíl v LS a AZ, prezenční/kombinovaný
		Generování karet cvičení (pro všechny zapsané studenty), možnost manuálního vytváření/mazání

#### Otázky:
	- (Zdroj dat)
	- Úvazkový list - B/C/D
	- XML/JSON - ukládání a načítání / ukládání
	- Rozsah dat u zaměstanců (telefon, email, kancelář)

#### Technologie:
	- C#/.NET
	- Desktopová aplikace (WinForms/WPF)?
	- JSON/XML/SQL (výběr)
	