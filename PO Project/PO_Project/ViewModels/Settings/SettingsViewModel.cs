﻿using MimeKit;
using MailKit.Net.Smtp;
using MailKit;
using OfficeOpenXml;
using PO_Project.Infrastructure.Repositories;
using PO_Project.Infrastructure.Services.Tags;
using PO_Project.Models.Empoyees.Models;
using PO_Project.Models.Settings;
using PO_Project.Models.Subjects;
using PO_Project.Models.Tags;
using PO_Project.Visualisation.Binding;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using PO_Project.Models;

namespace PO_Project.ViewModels.Settings
{
    public class SettingsViewModel : ViewModelBase
    {
        public Setting Setting { get; set; }

        private bool _selectedSemester;
        public bool SummerSemester
        {
            get
            {
                return !_selectedSemester;
            }
            set
            {
                if(value)
                {
                    _selectedSemester = false;
                    Setting.IsWinterSemeter = false;
                }

                OnPropertyChanged(nameof(SummerSemester));
            }
        }

        public bool WinterSemester
        {
            get
            {
                return _selectedSemester;
            }
            set
            {
                if (value)
                {
                    _selectedSemester = true;
                    Setting.IsWinterSemeter = true;
                }

                OnPropertyChanged(nameof(WinterSemester));
            }
        }

        private ICommand _exportAndSendTagsCommand;
        public ICommand ExportAndSendTags
        {
            get
            {
                if (_exportAndSendTagsCommand == null)
                {
                    _exportAndSendTagsCommand = new RelayCommand(
                        param => SendTags()
                    );
                }
                return _exportAndSendTagsCommand;
            }
        }

        private ICommand _deleteAllDataCommand;
        public ICommand DeleteAllDataCommand
        {
            get
            {
                if (_deleteAllDataCommand == null)
                {
                    _deleteAllDataCommand = new RelayCommand(
                        param => DataManager.Manager.DeleteAll()
                    );
                }
                return _deleteAllDataCommand;
            }
        }

        private bool _selectedStorageType;
        public bool UseXMLAsStorage
        {
            get
            {
                return _selectedStorageType;
            }
            set
            {
                if (value)
                {
                    _selectedStorageType = true;
                    Setting.DataStoringType = DataStoringTypes.XMLFile;
                }
                OnPropertyChanged(nameof(UseXMLAsStorage));
            }
        }

        public bool UseDatabaseAsStorage
        {
            get
            {
                return !_selectedStorageType;
            }
            set
            {
                if (value)
                {
                    _selectedStorageType = false;
                    Setting.DataStoringType = DataStoringTypes.Database;
                }
                OnPropertyChanged(nameof(UseDatabaseAsStorage));
            }
        }

        public SettingsViewModel()
        {
            Setting = DataManager.Manager.Setting;

            WinterSemester = Setting.IsWinterSemeter;
            SummerSemester = !Setting.IsWinterSemeter;

            if (Setting.DataStoringType == DataStoringTypes.XMLFile)
                UseXMLAsStorage = true;
        }

        public string ExportToExcel(Employee employee, List<Tag> tag)
        {
            var subjects = DataManager.Manager.AppData.Subjects;
            var fields = DataManager.Manager.AppData.Fields;
            var winterSemester = DataManager.Manager.Setting.IsWinterSemeter;

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            string sourceFile = "../uvazek.xlsx";
            string destinationFile = "../uvazek-" + employee.Name + "" + employee.Surname +".xlsx";

            try
            {
                File.Copy(sourceFile, destinationFile, true);
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }

            var file = new FileInfo(destinationFile);

            using(var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.FirstOrDefault(x => x.Name == "Employee");

                //Employee
                worksheet.Cells["L2"].Value = employee.Institute.ToString();
                worksheet.Cells["B3"].Value = employee.Name + " " + employee.Surname;

                if (employee.DoctoralStudent)
                    worksheet.Cells["F5"].Value = 0.5; //TODOTODOGKSEFGOBOSBGEJBLJEBLJBJLEBSLFJBLJBFLJ

                int tagCount = 0;
                for (int i = 11; i < 20; i++)
                {
                    if (tagCount < tag.Count)
                    {
                        worksheet.Cells["A" + i.ToString()].Value = tag[tagCount].ShortName;

                        if (winterSemester == true)
                            worksheet.Cells["C" + i.ToString()].Value = tag[tagCount].WeekCount;
                        else
                            worksheet.Cells["D" + i.ToString()].Value = tag[tagCount].WeekCount;

                        var subject = subjects.First(x => x.ID == tag[tagCount].SubjectID);

                        if(tag[tagCount].TypeOfTag == Models.TypesOfTag.Lecture)
                        {
                            worksheet.Cells["E" + i.ToString()].Value = subject.LectureHoursPerWeek;
                            if (worksheet.Cells["I" + i.ToString()].Value is not null)
                                worksheet.Cells["I" + i.ToString()].Value = (double)worksheet.Cells["I" + i.ToString()].Value + 1;
                            else
                                worksheet.Cells["I" + i.ToString()].Value = 1;
                        }

                        if (tag[tagCount].TypeOfTag == Models.TypesOfTag.Exercise)
                        {
                            worksheet.Cells["F" + i.ToString()].Value = subject.ExerciseHoursPerWeek;
                            if (worksheet.Cells["J" + i.ToString()].Value is not null)
                                worksheet.Cells["J" + i.ToString()].Value = (double)worksheet.Cells["J" + i.ToString()].Value + 1;
                            else
                                worksheet.Cells["J" + i.ToString()].Value = 1;
                        }

                        if (tag[tagCount].TypeOfTag == Models.TypesOfTag.Seminar)
                        {
                            worksheet.Cells["G" + i.ToString()].Value = subject.SeminarHoursPerWeek;
                            if (worksheet.Cells["K" + i.ToString()].Value is not null)
                                worksheet.Cells["K" + i.ToString()].Value = (double)worksheet.Cells["K" + i.ToString()].Value + 1;
                            else
                                worksheet.Cells["K" + i.ToString()].Value = 1;
                        }

                        tagCount++;
                    }
                    else
                        break;
                }

                tagCount = 0;
                for (int i = 25; i < 32; i++)
                {
                    if (tagCount < tag.Count)
                    {
                        worksheet.Cells["A" + i.ToString()].Value = tag[tagCount].ShortName;
                        
                        var subject = subjects.First(x => x.ID == tag[tagCount].SubjectID);

                        Debug.WriteLine("DEEEW IT");

                        if(subject.EndingType == Models.EndingTypes.ClassifiedCredit)
                            worksheet.Cells["B" + i.ToString()].Value = tag[tagCount].StudentCount;

                        if (subject.EndingType == Models.EndingTypes.Credit)
                            worksheet.Cells["C" + i.ToString()].Value = tag[tagCount].StudentCount;

                        if (subject.EndingType == Models.EndingTypes.Exam)
                            worksheet.Cells["D" + i.ToString()].Value = tag[tagCount].StudentCount;

                        tagCount++;
                    }
                    else
                        break;
                }

                package.SaveAsync();
            }

            return file.FullName;
        }

        public void SendEmail(string filePath, Employee employee)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Administrator", "admin@system.com"));
            message.To.Add(new MailboxAddress(employee.Name + " " + employee.Surname, employee.WorkEmail));
            message.Subject = "Teaching schedule";

            var bodyBuilder = new BodyBuilder();

            bodyBuilder.TextBody = @$"Hey {employee.Name},

                we are sending you your teaching plan. 

                -- Administrator";


            bodyBuilder.Attachments.Add(filePath);

            message.Body = bodyBuilder.ToMessageBody();

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Connect("127.0.0.1", 25, false);
                client.Send(message);
                client.Disconnect(true);
            }
        }

        public void SendTags()
        {
            var tags = DataManager.Manager.AppData.Tags;
            var employees = DataManager.Manager.AppData.Employees;

            foreach (var employee in employees)
            {
                List<Tag> employeeTags = new List<Tag>();

                foreach (var tag in tags)
                {
                    if (tag.EmployeeID == employee.ID)
                        employeeTags.Add(tag);
                }

                if (employeeTags.Count < 1)
                    continue;

                if (employee.WorkEmail is null || !IsValidEmail(employee.WorkEmail))
                {
                    Console.WriteLine("Email " + employee.WorkEmail + " not valid");
                    continue;
                }

                var path = ExportToExcel(employee, employeeTags);
                SendEmail(path, employee);
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
