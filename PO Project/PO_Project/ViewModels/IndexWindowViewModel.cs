﻿using PO_Project.ViewModels.Config.Navigators;
using PO_Project.Visualisation.Binding;

namespace PO_Project.ViewModels
{
    public class IndexWindowViewModel : ViewModelBase
    {
        public INavigator Navigator { get; set; }

        public IndexWindowViewModel(INavigator navigator)
        {
            Navigator = navigator;
        }
    }
}
