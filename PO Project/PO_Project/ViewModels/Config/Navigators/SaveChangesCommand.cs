﻿using PO_Project.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PO_Project.ViewModels.Config.Navigators
{
    public class SaveChangesCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public SaveChangesCommand()
        {
            
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Debug.WriteLine("Saving changes");

            DataManager.Manager.SaveData();
            DataManager.Manager.UpdateConfig();
        }
    }
}
