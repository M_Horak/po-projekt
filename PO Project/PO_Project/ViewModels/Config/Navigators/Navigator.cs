﻿using PO_Project.ViewModels.Config.Navigators.Commands;
using PO_Project.Visualisation.Binding;
using Ninject;
using System.Windows.Input;

namespace PO_Project.ViewModels.Config.Navigators
{
    public class Navigator : ViewModelBase, INavigator
    {
        private ViewModelBase _currentViewModel;
        public ViewModelBase CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        public ICommand UpdateCurrentViewModelCommand { get; set; }
        public ICommand SaveChangesCommand { get; set; }

        public Navigator(IKernel kernel)
        {
            UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(this, kernel);
            SaveChangesCommand = new SaveChangesCommand();
        }
    }
}
