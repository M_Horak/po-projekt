﻿namespace PO_Project.ViewModels.Config.Navigators.Enums
{
    public enum ViewType
    {
        HOME,
        GROUPS,
        FIELDS,
        SUBJECTS,
        EMPLOYEES,
        TAGS,
        SETTINGS
    }
}
