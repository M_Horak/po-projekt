﻿using PO_Project.Visualisation.Binding;
using System.Windows.Input;

namespace PO_Project.ViewModels.Config.Navigators
{
    public interface INavigator
    {
        ViewModelBase CurrentViewModel { get; set; }
        ICommand UpdateCurrentViewModelCommand { get; }
    }
}
