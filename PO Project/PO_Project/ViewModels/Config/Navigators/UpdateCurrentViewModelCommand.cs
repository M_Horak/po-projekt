﻿using PO_Project.Infrastructure.Services.Employees;
using PO_Project.ViewModels.Config.Navigators.Enums;
using Ninject;
using System;
using System.Windows.Input;
using PO_Project.ViewModels.Employees;
using PO_Project.ViewModels.Groups;
using PO_Project.Infrastructure.Services.Groups;
using PO_Project.ViewModels.Subjects;
using PO_Project.Infrastructure.Services.Subjects;
using PO_Project.ViewModels.Tags;
using PO_Project.Infrastructure.Services.Tags;
using PO_Project.ViewModels.Home;
using PO_Project.ViewModels.Settings;
using PO_Project.ViewModels.Fields;
using PO_Project.Infrastructure.Services.Fields;

namespace PO_Project.ViewModels.Config.Navigators.Commands
{
    public class UpdateCurrentViewModelCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly INavigator _navigator;
        private readonly IKernel _kernel;

        public UpdateCurrentViewModelCommand(INavigator navigator, IKernel kernel)
        {
            _navigator = navigator;
            _kernel = kernel;

            _navigator.CurrentViewModel = new HomeViewModel();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter is ViewType viewType)
            {
                switch (viewType)
                {
                    case ViewType.HOME:
                        _navigator.CurrentViewModel = new HomeViewModel();
                        break;
                    case ViewType.GROUPS:
                        _navigator.CurrentViewModel = new GroupViewModel(_kernel.Get<IGroupService>());
                        break;
                    case ViewType.FIELDS:
                        _navigator.CurrentViewModel = new FieldViewModel(_kernel.Get<IFieldService>());
                        break;
                    case ViewType.SUBJECTS:
                        _navigator.CurrentViewModel = new SubjectViewModel(_kernel.Get<ISubjectService>());
                        break;
                    case ViewType.EMPLOYEES:
                        _navigator.CurrentViewModel = new EmployeeViewModel(_kernel.Get<IEmployeeService>());
                        break;
                    case ViewType.TAGS:
                        _navigator.CurrentViewModel = new TagViewModel(_kernel.Get<ITagService>());
                        break;
                    case ViewType.SETTINGS:
                        _navigator.CurrentViewModel = new SettingsViewModel();
                        break;
                }
            }
        }
    }
}
