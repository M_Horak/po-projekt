﻿using PO_Project.Infrastructure.Repositories;
using PO_Project.Infrastructure.Services.Employees;
using PO_Project.Models.Empoyees.Models;
using PO_Project.Models.Tags;
using PO_Project.Visualisation.Binding;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;

namespace PO_Project.ViewModels.Employees
{
    public class EmployeeViewModel : ViewModelBase
    {
        private readonly IEmployeeService _service;
        public ObservableCollection<Employee> Employees { get; set; }
        public ObservableCollection<Tag> Tags { get; set; }

        public ObservableCollection<Tag> EmployeeTags { get; set; }

        private Employee _selectedEmployee;
        public Employee SelectedEmployee {
            get
            {
                return _selectedEmployee;
            }
            set
            {
                _selectedEmployee = value;
                OnPropertyChanged(nameof(SelectedEmployee));

                EmployeeTags.Clear();

                if(SelectedEmployee is not null)
                {
                    foreach (var item in Tags)
                    {
                        if (item.EmployeeID == SelectedEmployee.ID)
                        {
                            EmployeeTags.Add(item);
                        }
                    }
                }
                
                OnPropertyChanged(nameof(EmployeeTags));
            }
        
        }

        public string EditButtonText { get; set; }

        private bool _canBeEdited;
        public bool CanBeEdited { 
            get { return _canBeEdited; } 
            set
            {
                _canBeEdited = value;
                OnPropertyChanged(nameof(CanBeEdited));

                if (value)
                {
                    EditButtonText = "Save";
                }
                else
                {
                    EditButtonText = "Edit";
                }
                OnPropertyChanged(nameof(EditButtonText));
            } 
        }

        private ICommand _enableEditCommand;
        public ICommand EnableEditCommand
        {
            get
            {
                if (_enableEditCommand == null)
                {
                    _enableEditCommand = new RelayCommand(
                        param => EnableEdit()
                    );
                }
                return _enableEditCommand;
            }
        }

        private ICommand _newItemCommand;
        public ICommand NewItemCommand
        {
            get
            {
                if (_newItemCommand == null)
                {
                    _newItemCommand = new RelayCommand(
                        param => AddItem()
                    );
                }
                return _newItemCommand;
            }
        }

        private ICommand _removeItemCommand;
        public ICommand RemoveItemCommand
        {
            get
            {
                if (_removeItemCommand == null)
                {
                    _removeItemCommand = new RelayCommand(
                        param => RemoveItem()
                    );
                }
                return _removeItemCommand;
            }
        }

        public EmployeeViewModel(IEmployeeService service)
        {
            _service = service;

            Employees = DataManager.Manager.AppData.Employees;
            Tags = DataManager.Manager.AppData.Tags;
            EmployeeTags = new ObservableCollection<Tag>();

            CanBeEdited = false;
        }

        public void EnableEdit()
        {
            if(SelectedEmployee is null)
            {
                return;
            }

            if(CanBeEdited)
            {
                DataManager.Manager.AppData.Employees = Employees; //Save to DataManager
            }

            CanBeEdited = !CanBeEdited;
        }

        public void AddItem()
        {
            Employee newEmployee = new()
            {
                ID = DataManager.Manager.AppData.EmployeesLastID++
            };

            SelectedEmployee = newEmployee;
            Employees.Add(SelectedEmployee);

            CanBeEdited = true;
        }

        public void RemoveItem()
        {
            Employees.Remove(SelectedEmployee);
            SelectedEmployee = null;

            CanBeEdited = false;
        }
    }
}
