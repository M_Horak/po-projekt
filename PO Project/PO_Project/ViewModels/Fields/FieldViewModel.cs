﻿using PO_Project.Infrastructure.Repositories;
using PO_Project.Infrastructure.Services.Fields;
using PO_Project.Models.Fields;
using PO_Project.Models.Subjects;
using PO_Project.Visualisation.Binding;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PO_Project.ViewModels.Fields
{
    public class FieldViewModel : ViewModelBase
    {
        private readonly IFieldService _service;

        public ObservableCollection<Field> Fields { get; set; }

        //public ObservableCollection<Subject> Subjects { get; set; }

        private ObservableCollection<Subject> _selectedSubjects;
        public ObservableCollection<Subject> SelectedSubjects
        { 
            get => _selectedSubjects;
            set {
                _selectedSubjects = value;
                OnPropertyChanged(nameof(SelectedSubjects));
            } 
        }

        private Field _selectedField;
        public Field SelectedField
        {
            get
            {
                return _selectedField;
            }
            set
            {
                _selectedField = value;
                OnPropertyChanged(nameof(SelectedField));

                SelectedSubjects.Clear();

                if (_selectedField is null)
                    return;

                if (_selectedField.AssignedSubjects is null)
                    _selectedField.AssignedSubjects = new List<int>();

                foreach (var item in DataManager.Manager.AppData.Subjects)
                {
                    if (_selectedField.AssignedSubjects.Contains(item.ID))
                    {
                        Debug.WriteLine("YES");
                        SelectedSubjects.Add(item);
                    }
                }
                //OnPropertyChanged(nameof(SelectedSubjects));
            }

        }

        public string EditButtonText { get; set; }

        private bool _canBeEdited;
        public bool CanBeEdited
        {
            get { return _canBeEdited; }
            set
            {
                _canBeEdited = value;
                OnPropertyChanged(nameof(CanBeEdited));

                if (value)
                {
                    EditButtonText = "Save";
                }
                else
                {
                    EditButtonText = "Edit";
                }
                OnPropertyChanged(nameof(EditButtonText));
            }
        }

        private ICommand _enableEditCommand;
        public ICommand EnableEditCommand
        {
            get
            {
                if (_enableEditCommand == null)
                {
                    _enableEditCommand = new RelayCommand(
                        param => EnableEdit()
                    );
                }
                return _enableEditCommand;
            }
        }

        private ICommand _newItemCommand;
        public ICommand NewItemCommand
        {
            get
            {
                if (_newItemCommand == null)
                {
                    _newItemCommand = new RelayCommand(
                        param => AddItem()
                    );
                }
                return _newItemCommand;
            }
        }

        private ICommand _removeItemCommand;
        public ICommand RemoveItemCommand
        {
            get
            {
                if (_removeItemCommand == null)
                {
                    _removeItemCommand = new RelayCommand(
                        param => RemoveItem()
                    );
                }
                return _removeItemCommand;
            }
        }

        public FieldViewModel(IFieldService service)
        {
            _service = service;

            Fields = DataManager.Manager.AppData.Fields;
            SelectedSubjects = new ObservableCollection<Subject>();

            CanBeEdited = false;
        }

        public void EnableEdit()
        {
            if (SelectedField is null)
            {
                return;
            }

            if (CanBeEdited)
            {
                DataManager.Manager.AppData.Fields = Fields; //Save to DataManager
            }

            CanBeEdited = !CanBeEdited;
        }

        public void AddItem()
        {
            Field newField = new()
            {
                ID = DataManager.Manager.AppData.FieldsLastID++
            };

            SelectedField = newField;
            Fields.Add(SelectedField);

            CanBeEdited = true;
        }

        public void RemoveItem()
        {
            Fields.Remove(SelectedField);
            SelectedField = null;

            CanBeEdited = false;
        }
    }
}
