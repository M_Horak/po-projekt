﻿using PO_Project.Infrastructure.Services.Groups;
using PO_Project.Visualisation.Binding;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.ViewModels.Groups
{
    public class GroupViewModel : ViewModelBase
    {
        private readonly IGroupService _service;
        public ObservableCollection<GroupViewModel> Groups { get; set; }

        public GroupViewModel(IGroupService service)
        {
            _service = service;
        }
    }
}
