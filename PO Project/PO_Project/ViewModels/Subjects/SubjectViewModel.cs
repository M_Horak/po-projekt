﻿using PO_Project.Infrastructure.Repositories;
using PO_Project.Infrastructure.Services.Subjects;
using PO_Project.Models.Fields;
using PO_Project.Models.Subjects;
using PO_Project.Visualisation.Binding;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PO_Project.ViewModels.Subjects
{
    public class FieldWithSelector
    {
        public Field Field { get; set; }
        public bool IsSelected { get; set; }
    }

    public class SubjectViewModel : ViewModelBase
    {
        private readonly ISubjectService _service;
        public ObservableCollection<Subject> Subjects { get; set; }
        public ObservableCollection<Field> Fields { get; set; }

        private ObservableCollection<FieldWithSelector> _fieldWithSelector;
        public ObservableCollection<FieldWithSelector> FieldWithSelector
        {
            get => _fieldWithSelector;
            set
            {
                _fieldWithSelector = value;
                OnPropertyChanged(nameof(FieldWithSelector));
            }
        }

        private Subject _selectedSubject;
        public Subject SelectedSubject
        {
            get
            {
                return _selectedSubject;
            }
            set
            {
                _selectedSubject = value;
                OnPropertyChanged(nameof(SelectedSubject));

                RefreshFields();
            }
        }

        private void RefreshFields()
        {
            Debug.WriteLine("RUN");
            FieldWithSelector.Clear();
            foreach (var item in Fields)
            {
                if(_selectedSubject is not null && item.AssignedSubjects is not null)
                {
                    if (item.AssignedSubjects.Contains(_selectedSubject.ID))
                    {
                        FieldWithSelector.Add(new FieldWithSelector
                        {
                            Field = item,
                            IsSelected = true
                        });
                        Debug.WriteLine("TRUE");

                        continue;
                    }
                }

                FieldWithSelector.Add(new FieldWithSelector
                {
                    Field = item,
                    IsSelected = false
                });
                Debug.WriteLine("FALSE");
            }
        }

        private FieldWithSelector _selectedField;
        public FieldWithSelector SelectedField
        {
            get
            {
                return _selectedField;
            }
            set
            {
                _selectedField = value;
                OnPropertyChanged(nameof(SelectedField));
            }
        }

        public string EditButtonText { get; set; }

        private bool _canBeEdited;
        public bool CanBeEdited
        {
            get { return _canBeEdited; }
            set
            {
                _canBeEdited = value;
                OnPropertyChanged(nameof(CanBeEdited));

                if (value)
                {
                    EditButtonText = "Save";
                }
                else
                {
                    EditButtonText = "Edit";
                }
                OnPropertyChanged(nameof(EditButtonText));
            }
        }

        private ICommand _enableEditCommand;
        public ICommand EnableEditCommand
        {
            get
            {
                if (_enableEditCommand == null)
                {
                    _enableEditCommand = new RelayCommand(
                        param => EnableEdit()
                    );
                }
                return _enableEditCommand;
            }
        }

        private ICommand _newItemCommand;
        public ICommand NewItemCommand
        {
            get
            {
                if (_newItemCommand == null)
                {
                    _newItemCommand = new RelayCommand(
                        param => AddItem()
                    );
                }
                return _newItemCommand;
            }
        }

        private ICommand _removeItemCommand;
        public ICommand RemoveItemCommand
        {
            get
            {
                if (_removeItemCommand == null)
                {
                    _removeItemCommand = new RelayCommand(
                        param => RemoveItem()
                    );
                }
                return _removeItemCommand;
            }
        }

        private ICommand _assignSubjectCommand;
        public ICommand AssignSubjectCommand
        {
            get
            {
                if (_assignSubjectCommand == null)
                {
                    _assignSubjectCommand = new RelayCommand(
                        param => AssignSubject()
                    );
                }
                return _assignSubjectCommand;
            }
        }

        private ICommand _unassignSubjectCommand;
        public ICommand UnassignSubjectCommand
        {
            get
            {
                if (_unassignSubjectCommand == null)
                {
                    _unassignSubjectCommand = new RelayCommand(
                        param => UnassignSubject()
                    );
                }
                return _unassignSubjectCommand;
            }
        }

        public SubjectViewModel(ISubjectService service)
        {
            _service = service;

            Subjects = DataManager.Manager.AppData.Subjects;
            Fields = DataManager.Manager.AppData.Fields;

            FieldWithSelector = new ObservableCollection<FieldWithSelector>();

            foreach (var item in Fields)
            {
                FieldWithSelector.Add(new FieldWithSelector
                {
                    Field = item,
                    IsSelected = false
                });
            }

            CanBeEdited = false;
        }

        public void EnableEdit()
        {
            if (SelectedSubject is null)
            {
                return;
            }

            if (CanBeEdited)
            {
                DataManager.Manager.AppData.Subjects = Subjects; //Save to DataManager
            }

            CanBeEdited = !CanBeEdited;
        }

        public void AddItem()
        {
            Subject newSubject = new()
            {
                ID = DataManager.Manager.AppData.SubjectLastID++
            };
            SelectedSubject = newSubject;
            Subjects.Add(SelectedSubject);

            CanBeEdited = true;
        }

        public void RemoveItem()
        {
            Subjects.Remove(SelectedSubject);
            SelectedSubject = null;

            CanBeEdited = false;
        }

        public void AssignSubject()
        {
            if (SelectedSubject is null || SelectedField is null)
                return;

            if (SelectedField.Field.AssignedSubjects.Contains(SelectedSubject.ID))
                return;

            //SelectedField.Field.AssignedSubjects.Add(SelectedSubject.ID);

            int dataIndex = DataManager.Manager.AppData.Fields.IndexOf(SelectedField.Field);
            //Fields[dataIndex].AssignedSubjects.Add(SelectedSubject.ID);
            

            DataManager.Manager.AppData.Fields[dataIndex].AssignedSubjects.Add(SelectedSubject.ID);
            RefreshFields();
        }

        public void UnassignSubject()
        {
            if (SelectedSubject is null || SelectedField is null)
                return;
            //SelectedField.Field.AssignedSubjects.Remove(SelectedSubject.ID);

            int dataIndex = DataManager.Manager.AppData.Fields.IndexOf(SelectedField.Field);
            //Fields[dataIndex].AssignedSubjects.Remove(SelectedSubject.ID);

            DataManager.Manager.AppData.Fields[dataIndex].AssignedSubjects.Remove(SelectedSubject.ID);
            RefreshFields();

        }
    }
}
