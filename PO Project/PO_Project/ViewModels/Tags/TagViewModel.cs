﻿using PO_Project.Infrastructure.Repositories;
using PO_Project.Infrastructure.Services.Tags;
using PO_Project.Models.Empoyees.Models;
using PO_Project.Models.Fields;
using PO_Project.Models.Settings;
using PO_Project.Models.Subjects;
using PO_Project.Models.Tags;
using PO_Project.Visualisation.Binding;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PO_Project.ViewModels.Tags
{
    public class SubjectWithSelector
    {
        public Subject Subject { get; set; }
        public bool IsSelected { get; set; }
    }

    public class EmployeeWithSelector
    {
        public Employee Employee { get; set; }
        public bool IsSelected { get; set; }
    }

    public class TagViewModel : ViewModelBase
    {
        private readonly ITagService _service;
        public ObservableCollection<Tag> Tags { get; set; }

        public ObservableCollection<Subject> Subjects { get; set; }
        public ObservableCollection<Employee> Employees { get; set; }

        public ObservableCollection<Field> Fields { get; set; }

        public Setting Setting { get; set; }

        private Tag _selectedTag;
        public Tag SelectedTag
        {
            get
            {
                return _selectedTag;
            }
            set
            {
                _selectedTag = value;
                OnPropertyChanged(nameof(SelectedTag));

                RefreshFields();
            }

        }

        private SubjectWithSelector _selectedSubject;
        public SubjectWithSelector SelectedSubject
        {
            get
            {
                return _selectedSubject;
            }
            set
            {
                _selectedSubject = value;
                OnPropertyChanged(nameof(SelectedSubject));
            }
        }

        private EmployeeWithSelector _selectedEmployee;
        public EmployeeWithSelector SelectedEmployee
        {
            get
            {
                return _selectedEmployee;
            }
            set
            {
                _selectedEmployee = value;
                OnPropertyChanged(nameof(SelectedEmployee));
            }
        }

        private ObservableCollection<SubjectWithSelector> _subjectWithSelector;
        public ObservableCollection<SubjectWithSelector> SubjectWithSelector
        {
            get => _subjectWithSelector;
            set
            {
                _subjectWithSelector = value;
                OnPropertyChanged(nameof(SubjectWithSelector));
            }
        }

        private void RefreshFields()
        {
            EmployeeWithSelector.Clear();
            SubjectWithSelector.Clear();
            foreach (var item in Subjects)
            {
                if (_selectedTag is null)
                    continue;

                if (item.ID == _selectedTag.SubjectID)
                {
                    SubjectWithSelector.Add(new SubjectWithSelector
                    {
                        Subject = item,
                        IsSelected = true
                    });
                }
                else
                {
                    SubjectWithSelector.Add(new SubjectWithSelector
                    {
                        Subject = item,
                        IsSelected = false
                    });
                }
            }

            foreach (var item in Employees)
            {
                if (_selectedTag is null)
                    continue;

                if (item.ID == _selectedTag.EmployeeID)
                {
                    EmployeeWithSelector.Add(new EmployeeWithSelector
                    {
                        Employee = item,
                        IsSelected = true
                    });
                }
                else
                {
                    EmployeeWithSelector.Add(new EmployeeWithSelector
                    {
                        Employee = item,
                        IsSelected = false
                    });
                }
            }
        }

        private ObservableCollection<EmployeeWithSelector> _employeeWithSelector;
        public ObservableCollection<EmployeeWithSelector> EmployeeWithSelector
        {
            get => _employeeWithSelector;
            set
            {
                _employeeWithSelector = value;
                OnPropertyChanged(nameof(EmployeeWithSelector));
            }
        }


        public string EditButtonText { get; set; }

        private bool _canBeEdited;
        public bool CanBeEdited
        {
            get { return _canBeEdited; }
            set
            {
                _canBeEdited = value;
                OnPropertyChanged(nameof(CanBeEdited));

                if (value)
                {
                    EditButtonText = "Save";
                }
                else
                {
                    EditButtonText = "Edit";
                }
                OnPropertyChanged(nameof(EditButtonText));
            }
        }

        private ICommand _enableEditCommand;
        public ICommand EnableEditCommand
        {
            get
            {
                if (_enableEditCommand == null)
                {
                    _enableEditCommand = new RelayCommand(
                        param => EnableEdit()
                    );
                }
                return _enableEditCommand;
            }
        }

        private ICommand _newItemCommand;
        public ICommand NewItemCommand
        {
            get
            {
                if (_newItemCommand == null)
                {
                    _newItemCommand = new RelayCommand(
                        param => AddItem()
                    );
                }
                return _newItemCommand;
            }
        }

        private ICommand _removeItemCommand;
        public ICommand RemoveItemCommand
        {
            get
            {
                if (_removeItemCommand == null)
                {
                    _removeItemCommand = new RelayCommand(
                        param => RemoveItem()
                    );
                }
                return _removeItemCommand;
            }
        }

        private ICommand _assignSubjectCommand;
        public ICommand AssignSubjectCommand
        {
            get
            {
                if (_assignSubjectCommand == null)
                {
                    _assignSubjectCommand = new RelayCommand(
                        param => AssignSubject()
                    );
                }
                return _assignSubjectCommand;
            }
        }

        private ICommand _unassignSubjectCommand;
        public ICommand UnassignSubjectCommand
        {
            get
            {
                if (_unassignSubjectCommand == null)
                {
                    _unassignSubjectCommand = new RelayCommand(
                        param => UnassignSubject()
                    );
                }
                return _unassignSubjectCommand;
            }
        }

        private ICommand _assignEmployeeCommand;
        public ICommand AssignEmployeeCommand
        {
            get
            {
                if (_assignEmployeeCommand == null)
                {
                    _assignEmployeeCommand = new RelayCommand(
                        param => AssignEmployee()
                    );
                }
                return _assignEmployeeCommand;
            }
        }

        private ICommand _unassignEmployeeCommand;
        public ICommand UnassignEmployeeCommand
        {
            get
            {
                if (_unassignEmployeeCommand == null)
                {
                    _unassignEmployeeCommand = new RelayCommand(
                        param => UnassignEmployee()
                    );
                }
                return _unassignEmployeeCommand;
            }
        }

        private ICommand _generateTagsCommand;
        public ICommand GenerateTagsCommand
        {
            get
            {
                if (_generateTagsCommand == null)
                {
                    _generateTagsCommand = new RelayCommand(
                        param => GenerateTags()
                    );
                }
                return _generateTagsCommand;
            }
        }

        private ICommand _recalculateTagsCommand;
        public ICommand RecalculateTagsCommand
        {
            get
            {
                if (_recalculateTagsCommand == null)
                {
                    _recalculateTagsCommand = new RelayCommand(
                        param => RecalculateTags()
                    );
                }
                return _recalculateTagsCommand;
            }
        }

        private ICommand _deleteAllTagsCommand;
        public ICommand DeleteAllTagsCommand
        {
            get
            {
                if (_deleteAllTagsCommand == null)
                {
                    _deleteAllTagsCommand = new RelayCommand(
                        param => DeleteAllTags()
                    );
                }
                return _deleteAllTagsCommand;
            }
        }

        public TagViewModel(ITagService service)
        {
            _service = service;

            Tags = DataManager.Manager.AppData.Tags;
            Subjects = DataManager.Manager.AppData.Subjects;
            Employees = DataManager.Manager.AppData.Employees;
            Fields = DataManager.Manager.AppData.Fields;
            Setting = DataManager.Manager.Setting;

            SubjectWithSelector = new ObservableCollection<SubjectWithSelector>();
            foreach (var item in Subjects)
            {
                SubjectWithSelector.Add(new SubjectWithSelector
                {
                    Subject = item,
                    IsSelected = false
                });
            }

            EmployeeWithSelector = new ObservableCollection<EmployeeWithSelector>();
            foreach (var item in Employees)
            {
                EmployeeWithSelector.Add(new EmployeeWithSelector
                {
                    Employee = item,
                    IsSelected = false
                });
            }

            CanBeEdited = false;
        }

        public void EnableEdit()
        {
            if (SelectedTag is null)
            {
                return;
            }

            if (CanBeEdited)
            {
                DataManager.Manager.AppData.Tags = Tags; //Save to DataManager
            }

            CanBeEdited = !CanBeEdited;
        }

        public void AddItem()
        {
            Tag newTag = new()
            {
                ID = DataManager.Manager.AppData.TagsLastID++,
                SubjectID = null,
                EmployeeID = null
            };

            SelectedTag = newTag;
            Tags.Add(SelectedTag);

            CanBeEdited = true;
        }

        public void RemoveItem()
        {
            Tags.Remove(SelectedTag);
            SelectedTag = null;

            CanBeEdited = false;
        }

        public void AssignSubject()
        {
            Debug.WriteLine("RRRUN");
            if (SelectedSubject is null || SelectedTag is null)
                return;

            if (SelectedTag.SubjectID == SelectedSubject.Subject.ID)
                return;

            int dataIndex = DataManager.Manager.AppData.Tags.IndexOf(SelectedTag);

            DataManager.Manager.AppData.Tags[dataIndex].SubjectID = SelectedSubject.Subject.ID;
            RefreshFields();
        }

        public void UnassignSubject()
        {
            if (SelectedSubject is null || SelectedTag is null)
                return;

            int dataIndex = DataManager.Manager.AppData.Tags.IndexOf(SelectedTag);

            DataManager.Manager.AppData.Tags[dataIndex].SubjectID = null;

            if(SelectedEmployee.Employee.Obligation > 0)
                SelectedEmployee.Employee.Obligation -= CalculateWorkPoints(SelectedTag);
            RefreshFields();

        }

        public void AssignEmployee()
        {
            if (SelectedEmployee is null || SelectedTag is null)
                return;

            if (SelectedTag.EmployeeID == SelectedEmployee.Employee.ID)
                return;

            int dataIndex = DataManager.Manager.AppData.Tags.IndexOf(SelectedTag);

            DataManager.Manager.AppData.Tags[dataIndex].EmployeeID = SelectedEmployee.Employee.ID;

            SelectedEmployee.Employee.Obligation += CalculateWorkPoints(SelectedTag);
            RefreshFields();
        }

        public int CalculateWorkPoints(Tag tag)
        {
            var subject = Subjects.First(x => x.ID == tag.SubjectID);

            int points = 0;
            switch (tag.TypeOfTag)
            {
                case Models.TypesOfTag.Lecture:
                    points = tag.WeekCount * subject.LectureHoursPerWeek;
                    break;
                case Models.TypesOfTag.Seminar:
                    points = tag.WeekCount * subject.SeminarHoursPerWeek;
                    break;
                case Models.TypesOfTag.Exercise:
                    points = tag.WeekCount * subject.ExerciseHoursPerWeek;
                    break;
                default:
                    points = -1;
                    break;
            }

            return points;
        }

        public void UnassignEmployee()
        {
            if (SelectedEmployee is null || SelectedTag is null)
                return;

            int dataIndex = DataManager.Manager.AppData.Tags.IndexOf(SelectedTag);

            DataManager.Manager.AppData.Tags[dataIndex].EmployeeID = null;
            RefreshFields();
        }

        public void GenerateTags()
        {
            Debug.WriteLine("GENERATING TAGS");

            foreach (var field in Fields)
            {
                Debug.WriteLine("A");

                if (Setting.IsWinterSemeter)
                {
                    if (field.Semester != Models.Semesters.Winter)
                        continue;
                }
                else
                {
                    if (field.Semester != Models.Semesters.Summer)
                        continue;
                }

                Debug.WriteLine("B");

                foreach (var item in Subjects)
                {
                    if (field.AssignedSubjects is null)
                        field.AssignedSubjects = new List<int>();

                    if (!field.AssignedSubjects.Contains(item.ID))
                        continue;

                    Debug.WriteLine("YEY");

                    if (item.LectureHoursPerWeek > 0)
                    {
                        Debug.WriteLine("LECTURES");

                        Tag tag = new()
                        {
                            ShortName = field.ShortName + "-Lec-" + item.ShortName,
                            FullName = field.FullName + " Lecture - " + item.FullName,
                            StudentCount = field.StudentCount,
                            Language = field.Language,
                            TypeOfTag = Models.TypesOfTag.Lecture,
                            EmployeeID = null,
                            IsException = false,
                            SubjectID = item.ID,
                            HoursCount = item.LectureHoursPerWeek * item.NumberOfWeeks,
                            WeekCount = item.NumberOfWeeks
                        };

                        if(Tags.FirstOrDefault(x => x.ShortName == tag.ShortName) is null)
                            Tags.Add(tag);
                    }
                    int tagNumber = 1;
                    for (int i = 0; i < field.StudentCount; i += item.ClassSize)
                    {
                        int studentCount = item.ClassSize;
                        

                        if (i + item.ClassSize > field.StudentCount)
                            studentCount = field.StudentCount - i;

                        if (item.SeminarHoursPerWeek > 0)
                        {
                            Debug.WriteLine("SEMINARS");

                            Tag tag = new()
                            {
                                ShortName = field.ShortName + "-Sem-" + item.ShortName + "-" + tagNumber.ToString(),
                                FullName = field.FullName + " Seminar - " + item.FullName + " " + tagNumber.ToString(),
                                StudentCount = studentCount,
                                Language = field.Language,
                                TypeOfTag = Models.TypesOfTag.Seminar,
                                EmployeeID = null,
                                IsException = false,
                                SubjectID = item.ID,
                                HoursCount = item.SeminarHoursPerWeek * item.NumberOfWeeks,
                                WeekCount = item.NumberOfWeeks
                            };
                            if (Tags.FirstOrDefault(x => x.ShortName == tag.ShortName) is null)
                                Tags.Add(tag);
                        }

                        if (item.ExerciseHoursPerWeek > 0)
                        {
                            Debug.WriteLine("EXERCISES");

                            Tag tag = new()
                            {
                                ShortName = field.ShortName + "-Exe-" + item.ShortName + "-" + tagNumber.ToString(),
                                FullName = field.FullName + " Exercise - " + item.FullName + " " + tagNumber.ToString(),
                                StudentCount = studentCount,
                                Language = field.Language,
                                TypeOfTag = Models.TypesOfTag.Exercise,
                                EmployeeID = null,
                                IsException = false,
                                SubjectID = item.ID,
                                HoursCount = item.ExerciseHoursPerWeek * item.NumberOfWeeks,
                                WeekCount = item.NumberOfWeeks
                            };
                            if (Tags.FirstOrDefault(x => x.ShortName == tag.ShortName) is null)
                                Tags.Add(tag);
                        }
                        tagNumber++;
                    }
                }
            }
            Debug.WriteLine("FINISHED");
        }

        public void RecalculateTags()
        {
            Debug.WriteLine("RECALCULATING TAGS");
            ObservableCollection<Tag> TagsNew = new();

            foreach (var item in Tags)
            {
                if (item.IsException)
                    TagsNew.Add(item);
            }

            foreach (var field in Fields)
            {
                if (Setting.IsWinterSemeter)
                {
                    if (field.Semester != Models.Semesters.Winter)
                        continue;
                }
                else
                {
                    if (field.Semester != Models.Semesters.Summer)
                        continue;
                }

                foreach (var item in Subjects)
                {
                    if (!field.AssignedSubjects.Contains(item.ID))
                        continue;

                    if (item.LectureHoursPerWeek > 0)
                    {
                        Debug.WriteLine("LECTURES");

                        Tag tag = new()
                        {
                            ShortName = field.ShortName + "-Lec-" + item.ShortName,
                            FullName = field.FullName + " Lecture - " + item.FullName,
                            StudentCount = field.StudentCount,
                            Language = field.Language,
                            TypeOfTag = Models.TypesOfTag.Lecture,
                            EmployeeID = null,
                            IsException = false,
                            SubjectID = item.ID,
                            HoursCount = item.LectureHoursPerWeek * item.NumberOfWeeks,
                            WeekCount = item.NumberOfWeeks
                        };

                        var prevTag = Tags.FirstOrDefault(x => x.ShortName == tag.ShortName);

                        if (prevTag is not null)
                        {
                            tag.EmployeeID = prevTag.EmployeeID;
                            tag.SubjectID = prevTag.SubjectID;
                        }
                        TagsNew.Add(tag);
                    }

                    int tagNumber = 1;
                    for (int i = 0; i < field.StudentCount; i += item.ClassSize)
                    {
                        int studentCount = item.ClassSize;

                        if (i + item.ClassSize > field.StudentCount)
                            studentCount = field.StudentCount - i;

                        if (item.SeminarHoursPerWeek > 0)
                        {
                            Debug.WriteLine("SEMINARS");

                            Tag tag = new()
                            {
                                ShortName = field.ShortName + "-Sem-" + item.ShortName + "-" + tagNumber.ToString(),
                                FullName = field.FullName + " Seminar - " + item.FullName + " " + tagNumber.ToString(),
                                StudentCount = studentCount,
                                Language = field.Language,
                                TypeOfTag = Models.TypesOfTag.Seminar,
                                EmployeeID = null,
                                IsException = false,
                                SubjectID = item.ID,
                                HoursCount = item.SeminarHoursPerWeek * item.NumberOfWeeks,
                                WeekCount = item.NumberOfWeeks
                            };

                            var prevTag = Tags.FirstOrDefault(x => x.ShortName == tag.ShortName);

                            if (prevTag is not null)
                            {
                                tag.EmployeeID = prevTag.EmployeeID;
                                tag.SubjectID = prevTag.SubjectID;
                            }
                            TagsNew.Add(tag);
                        }

                        if (item.ExerciseHoursPerWeek > 0)
                        {
                            Debug.WriteLine("EXERCISES");

                            Tag tag = new()
                            {
                                ShortName = field.ShortName + "-Exe-" + item.ShortName + "-" + tagNumber.ToString(),
                                FullName = field.FullName + " Exercise - " + item.FullName + " " + tagNumber.ToString(),
                                StudentCount = studentCount,
                                Language = field.Language,
                                TypeOfTag = Models.TypesOfTag.Exercise,
                                EmployeeID = null,
                                IsException = false,
                                SubjectID = item.ID,
                                HoursCount = item.ExerciseHoursPerWeek * item.NumberOfWeeks,
                                WeekCount = item.NumberOfWeeks
                            };

                            var prevTag = Tags.FirstOrDefault(x => x.ShortName == tag.ShortName);

                            if (prevTag is not null)
                            {
                                tag.EmployeeID = prevTag.EmployeeID;
                                tag.SubjectID = prevTag.SubjectID;
                            }
                            TagsNew.Add(tag);
                        }
                        tagNumber++;
                    }
                }
            }
            Tags.Clear();
            foreach (var item in TagsNew)
            {
                Tags.Add(item);
            }
        }

        public void DeleteAllTags()
        {
            Tags.Clear();

            foreach (var item in Employees)
            {
                item.Obligation = 0;
            }
        }
    }
}
