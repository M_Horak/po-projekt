﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Models.Tags
{
    public class Tag : Entity
    {
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public TypesOfTag TypeOfTag { get; set; }
        public int StudentCount { get; set; }
        public int HoursCount { get; set; }
        public int WeekCount { get; set; }
        public Languages Language { get; set; }

        public int? EmployeeID { get; set; }
        public int? SubjectID { get; set; }

        public bool IsException { get; set; }
    }
}
