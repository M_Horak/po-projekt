﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Models.Settings
{
    public class Setting : Entity
    {
        public DataStoringTypes DataStoringType { get; set; }
        public string XMLFilePath { get; set; }
        public bool IsWinterSemeter { get; set; }

        public Setting()
        {

        }
        public Setting(DataStoringTypes dataStoringType, string xMLFilePath, bool isWinterSemeter)
        {
            DataStoringType = dataStoringType;
            XMLFilePath = xMLFilePath;
            IsWinterSemeter = isWinterSemeter;
        }
    }
}
