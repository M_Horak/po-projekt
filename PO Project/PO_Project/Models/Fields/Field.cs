﻿using PO_Project.Models.Subjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Models.Fields
{
    public class Field : Entity
    {
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public int YearsOfStudy { get; set; }
        public Semesters Semester { get; set; }
        public int StudentCount { get; set; }
        public FormsOfStudies FormOfStudy { get; set; }
        public TypesOfStudies TypeOfStudy { get; set; }
        public Languages Language { get; set; }
        public List<int> AssignedSubjects { get; set; }

        public Field() { }

        public Field(string shortName, string fullName, int yearsOfStudy, Semesters semester, int studentCount, FormsOfStudies formOfStudy, TypesOfStudies typeOfStudy, Languages language)
        {
            ShortName = shortName;
            FullName = fullName;
            YearsOfStudy = yearsOfStudy;
            Semester = semester;
            StudentCount = studentCount;
            FormOfStudy = formOfStudy;
            TypeOfStudy = typeOfStudy;
            Language = language;
        }
    }
}
