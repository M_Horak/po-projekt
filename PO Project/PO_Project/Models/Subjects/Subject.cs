﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Models.Subjects
{

    public class Subject: Entity
    {
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public int NumberOfWeeks { get; set; }
        public int LectureHoursPerWeek { get; set; }
        public int SeminarHoursPerWeek { get; set; }
        public int ExerciseHoursPerWeek { get; set; }
        public EndingTypes EndingType { get; set; }
        public Languages Language { get; set; }
        public int ClassSize { get; set; }
        public int CreditCount { get; set; }
        public Institutes Institute { get; set; }

        public Subject()
        {

        }

        public Subject(string shortName, string fullName, int numberOfWeeks, int lectureHoursPerWeek, int seminarHoursPerWeek, int exerciseHoursPerWeek, EndingTypes endingType, Languages language, int classSize, int creditCount, Institutes institute)
        {
            ShortName = shortName;
            FullName = fullName;
            NumberOfWeeks = numberOfWeeks;
            LectureHoursPerWeek = lectureHoursPerWeek;
            SeminarHoursPerWeek = seminarHoursPerWeek;
            ExerciseHoursPerWeek = exerciseHoursPerWeek;
            EndingType = endingType;
            Language = language;
            ClassSize = classSize;
            CreditCount = creditCount;
            Institute = institute;
        }
    }
}
