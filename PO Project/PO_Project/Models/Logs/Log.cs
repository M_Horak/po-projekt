﻿using System;
using System.Collections.Generic;

namespace PO_Project.Models.Logs.models
{
    public class Log
    {
        public string Title { get; set; }
        public Tuple<Enum, IList<string>> Action { get; set; }
        public DateTime StartTime { get; set; }
    }
}