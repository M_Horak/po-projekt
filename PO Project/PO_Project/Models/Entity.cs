﻿using System;

namespace PO_Project.Models
{
    public class Entity
    {
        public int ID { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}