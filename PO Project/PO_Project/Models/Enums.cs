﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Models
{
    public enum Semesters { Winter, Summer }
    public enum FormsOfStudies { FullTime, Distance, Combined }
    public enum TypesOfStudies { Bachelor, Magister, Doctorant}
    public enum TypesOfTag { Lecture, Seminar, Exercise }
    public enum Languages { Czech, English }
    public enum EndingTypes { Credit, Exam, ClassifiedCredit }
    public enum Institutes { UIUI, UPKS, UART, UEM, UBR, UM}
    public enum DataStoringTypes { Database, XMLFile }
}
