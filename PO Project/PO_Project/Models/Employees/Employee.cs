﻿using PO_Project.Models.Tags;
using System.Collections.Generic;

namespace PO_Project.Models.Empoyees.Models
{
    public class Employee : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public Institutes Institute { get; set; }
        public string WorkEmail { get; set; }
        public string PrivateEmail { get; set; }
        public string WorkPhone { get; set; }
        public string PrivatePhone { get; set; }
        public bool DoctoralStudent { get; set; }
        public double Obligation { get; set; }

        public List<Tag> Tags { get; set; }

        public Employee() { }

        public Employee(string name, string surname, Institutes institute, string workEmail, string privateEmail, string workPhone, string privatePhone, bool doctoralStudent, double obligation, List<Tag> tags)
        {
            Name = name;
            Surname = surname;
            Institute = institute;
            WorkEmail = workEmail;
            PrivateEmail = privateEmail;
            WorkPhone = workPhone;
            PrivatePhone = privatePhone;
            DoctoralStudent = doctoralStudent;
            Obligation = obligation;
            Tags = tags;
        }
    }
}
