﻿using PO_Project.Infrastructure.Repositories.Subjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Services.Subjects
{
    public class SubjectService : ISubjectService
    {
        private readonly ISubjectsRepository _subjectsRepository;
        public SubjectService(ISubjectsRepository subjectsRepository)
        {
            _subjectsRepository = subjectsRepository;
        }
    }
}
