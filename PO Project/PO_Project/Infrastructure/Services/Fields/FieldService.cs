﻿using PO_Project.Infrastructure.Repositories.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Services.Fields
{
    public class FieldService : IFieldService
    {
        private readonly IFieldsRespository _fieldsRepository;
        public FieldService(IFieldsRespository fieldsRepository)
        {
            _fieldsRepository = fieldsRepository;
        }
    }
}
