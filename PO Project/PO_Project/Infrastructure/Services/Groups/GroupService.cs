﻿using PO_Project.Infrastructure.Repositories.Groups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Services.Groups
{
    public class GroupService : IGroupService
    {
        private readonly IGroupsRepository _groupsRepository;
        public GroupService(IGroupsRepository groupsRepository)
        {
            _groupsRepository = groupsRepository;
        }
    }
}
