﻿using PO_Project.Infrastructure.Repositories;
using PO_Project.Infrastructure.Repositories.Tags;
using PO_Project.Models;
using PO_Project.Models.Fields;
using PO_Project.Models.Subjects;
using PO_Project.Models.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Services.Tags
{
    public class TagService : ITagService
    {
        public readonly ITagsRepository _tagRepository;
        public TagService(ITagsRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }

        public void CalculateTags(Subject subject)
        {
            Field field = DataManager.Manager.AppData.Fields.First(item => item.AssignedSubjects.Contains(subject.ID));
            //Student count, class size, subject (lectures, exercises,...)
            int classSize = subject.ClassSize;
            int studentCount = field.StudentCount;
            List<int> splits = GetClassSize(studentCount, classSize);

            //Lecture
            if (subject.LectureHoursPerWeek > 0)
            {
                DataManager.Manager.AppData.Tags.Add(new Tag
                {
                    ShortName = subject.ShortName + "-L",
                    FullName = subject.FullName + " - Lecture",
                    StudentCount = studentCount,
                    HoursCount = subject.LectureHoursPerWeek,
                    WeekCount = subject.NumberOfWeeks,
                    Language = subject.Language,
                    TypeOfTag = TypesOfTag.Lecture,
                    SubjectID = subject.ID
                });
            }

            //Seminar
            if (subject.SeminarHoursPerWeek > 0)
            {
                foreach (var item in splits)
                {
                    DataManager.Manager.AppData.Tags.Add(new Tag
                    {
                        ShortName = subject.ShortName + "-S",
                        FullName = subject.FullName + " - Seminar",
                        StudentCount = studentCount,
                        HoursCount = subject.SeminarHoursPerWeek,
                        WeekCount = subject.NumberOfWeeks,
                        Language = subject.Language,
                        TypeOfTag = TypesOfTag.Seminar,
                        SubjectID = subject.ID
                    });

                }
            }

            //Exercise
            if (subject.ExerciseHoursPerWeek > 0)
            {
                foreach (var item in splits)
                {
                    DataManager.Manager.AppData.Tags.Add(new Tag
                    {
                        ShortName = subject.ShortName + "-E",
                        FullName = subject.FullName + " - Exercise",
                        StudentCount = studentCount,
                        HoursCount = subject.ExerciseHoursPerWeek,
                        WeekCount = subject.NumberOfWeeks,
                        Language = subject.Language,
                        TypeOfTag = TypesOfTag.Exercise,
                        SubjectID = subject.ID
                    });

                }
            }
        }

        List<int> GetClassSize(int numberToSplit, int maxSize)
        {
            List<int> result = new List<int>();

            if (numberToSplit < maxSize)
            {
                result.Add(numberToSplit);
            }
            else if (numberToSplit % maxSize == 0)
            {
                for (int i = 0; i < maxSize; i++)
                {
                    result.Add(numberToSplit / maxSize);
                }
            }
            else
            {
                int zp = maxSize - (numberToSplit % maxSize);
                int pp = numberToSplit / maxSize;
                for (int i = 0; i < maxSize; i++)
                {

                    if (i >= zp)
                        result.Add(pp + 1);
                    else
                        result.Add(pp);
                }
            }

            return result;
        }
    }
}
