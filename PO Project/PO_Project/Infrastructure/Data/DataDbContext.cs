﻿using PO_Project.Models.Empoyees.Models;
using PO_Project.Models.Fields;
using PO_Project.Models.Subjects;
using PO_Project.Models.Tags;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Data
{
    public class DataDbContext : DbContext
    {
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public DataDbContext(string connectionString) : base(connectionString)
        {
            //Database.SetInitializer<DataDbContext>(null);
        }
    }
}
