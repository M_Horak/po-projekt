﻿using PO_Project.Models;
using PO_Project.Models.Empoyees.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using PO_Project.Models.Subjects;
using PO_Project.Models.Tags;
using PO_Project.Models.Groups;
using System.Collections.ObjectModel;
using PO_Project.Models.Fields;
using PO_Project.Models.Settings;
using System.ComponentModel;

namespace PO_Project.Infrastructure.Data
{
    public class DataContext
    {
        public ObservableCollection<Subject> Subjects { get; set; }
        public ObservableCollection<Employee> Employees { get; set; }
        public ObservableCollection<Field> Fields { get; set; }
        public ObservableCollection<Tag> Tags { get; set; }

        public int SubjectLastID { get; set; }
        public int EmployeesLastID { get; set; }
        public int FieldsLastID { get; set; }
        public int TagsLastID { get; set; }

        public DataContext()
        {
            Subjects = new ObservableCollection<Subject>();
            Employees = new ObservableCollection<Employee>();
            Fields = new ObservableCollection<Field>();
            Tags = new ObservableCollection<Tag>();
        }
    }
}
