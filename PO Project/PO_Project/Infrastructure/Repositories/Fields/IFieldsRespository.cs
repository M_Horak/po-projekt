﻿using PO_Project.Models.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Repositories.Fields
{
    public interface IFieldsRespository : IRepositoryBase<Field>
    {
    }
}
