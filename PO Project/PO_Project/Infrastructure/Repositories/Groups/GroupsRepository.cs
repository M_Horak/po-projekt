﻿using PO_Project.Infrastructure.Data;
using PO_Project.Models.Groups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Repositories.Groups
{
    public class GroupsRepository : IGroupsRepository
    {
        private readonly DataContext _dataContext;
        public GroupsRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<Group> GetAll(Func<Group, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Group Insert(Group entity)
        {
            throw new NotImplementedException();
        }

        public Group Remove(Group entity)
        {
            throw new NotImplementedException();
        }
    }
}
