﻿using PO_Project.Models.Groups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Repositories.Groups
{
    public interface IGroupsRepository : IRepositoryBase<Group>
    {
    }
}
