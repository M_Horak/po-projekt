﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using PO_Project.Infrastructure.Data;
using System.Configuration;
using System.Diagnostics;
using PO_Project.Models.Settings;
using PO_Project.Models;
using System.Data.Entity.Migrations;

namespace PO_Project.Infrastructure.Repositories
{
    public class DataManager
    {
        public DataContext AppData { get; set; }

        public DataDbContext AppDb { get; set; }

        private readonly string defaultPath = "../AppData.xml";
        private readonly string defaultPathDb = "../AppDb.mdf";
        private readonly string defaultFullPathDb = "";

        private readonly bool winterSemester = true;

        public Setting Setting { get; set; }

        private static DataManager _dataManager;

        public static DataManager Manager
        {
            get
            {
                if (_dataManager is null)
                    _dataManager = new DataManager();

                return _dataManager;
            }
        }

        public DataManager()
        {
            Setting = new Setting
            {
                XMLFilePath = GetConfig("XMLFilePath")
            };

            if (Setting.XMLFilePath is null)
            {
                SetConfig("XMLFilePath", defaultPath);
                Setting.XMLFilePath = defaultPath;
            }

            var semester = GetConfig("IsWinterSemester");

            if (semester is not null)
            {
                if (semester == "True")
                    Setting.IsWinterSemeter = true;
                else
                    Setting.IsWinterSemeter = false;
            }
            else
            {
                SetConfig("ISWinterSemester", winterSemester.ToString());
                Setting.IsWinterSemeter = true;
            }

            var dataStorageType = GetConfig("DataStorageType");

            if (dataStorageType is not null)
            {
                if (dataStorageType == "XMLFile")
                    Setting.DataStoringType = DataStoringTypes.XMLFile;
                else
                    Setting.DataStoringType = DataStoringTypes.Database;
            }
            else
            {
                SetConfig("DataStorageType", DataStoringTypes.XMLFile.ToString());
                Setting.DataStoringType = DataStoringTypes.XMLFile;
            }

            var databaseFile = new FileInfo(defaultPathDb);
            defaultFullPathDb = databaseFile.FullName;

            if (Setting.DataStoringType == DataStoringTypes.XMLFile)
            {
                Debug.WriteLine("DESERIALIZING");
                Deserialize();
            }
            else
            {
                Debug.WriteLine("DATABAZING");
                LoadFromDatabase();
            }

        }

        public void SaveData()
        {
            Console.WriteLine("SAVING DATA");

            if (Setting.DataStoringType == DataStoringTypes.XMLFile)
            {
                Debug.WriteLine("Serializing");
                Serialize();
            }
            else
            {
                Debug.WriteLine("Saving to database");
                SaveToDatabase();
            }
        }

        public void Serialize()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DataContext));
            var stream = new FileStream(Setting.XMLFilePath, FileMode.Create);

            serializer.Serialize(stream, AppData);

            stream.Close();
        }

        public void Deserialize()
        {
            if (File.Exists(Setting.XMLFilePath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(DataContext));
                var stream = new FileStream(Setting.XMLFilePath, FileMode.Open);

                AppData = (DataContext)serializer.Deserialize(stream);
                Setting.XMLFilePath = stream.Name;
                stream.Close();
            }
            else
            {
                AppData = new DataContext();
                SaveData();
            }

        }

        public void SaveToDatabase()
        {
            if(AppDb is null)
            {
                AppDb = new DataDbContext($"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename={defaultFullPathDb};Integrated Security=True;Connect Timeout=30");
                AppDb.Database.CreateIfNotExists();
            }


            AppDb.Fields.AddOrUpdate(AppData.Fields.ToArray());
            AppDb.Subjects.AddOrUpdate(AppData.Subjects.ToArray());
            AppDb.Tags.AddOrUpdate(AppData.Tags.ToArray());
            AppDb.Employees.AddOrUpdate(AppData.Employees.ToArray());

            AppDb.SaveChanges();
        }

        public void LoadFromDatabase()
        {
            AppDb = new DataDbContext($"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename={defaultFullPathDb};Integrated Security=True;Connect Timeout=30");
            AppDb.Database.CreateIfNotExists();

            AppData = new DataContext
            {
                Employees = ConvertToOC(AppDb.Employees.ToList()),
                Fields = ConvertToOC(AppDb.Fields.ToList()),
                Subjects = ConvertToOC(AppDb.Subjects.ToList()),
                Tags = ConvertToOC(AppDb.Tags.ToList())
            };
        }

        public void DeleteAll()
        {
            AppData.Employees.Clear();
            AppData.Fields.Clear();
            AppData.Subjects.Clear();
            AppData.Tags.Clear();

            SaveData();
        }

        public string GetConfig(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? null;
                Debug.WriteLine(result);
                return result;
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("Error reading app settings");
                return null;
            }
        }

        public void SetConfig(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                Debug.WriteLine(configFile.FilePath);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Debug.WriteLine("Error writing app settings");
            }
        }

        public void UpdateConfig()
        {
            SetConfig("XMLFilePath", Setting.XMLFilePath);
            SetConfig("IsWinterSemester", Setting.IsWinterSemeter.ToString());
            SetConfig("DataStorageType", Setting.DataStoringType.ToString());
        }
        public static ObservableCollection<T> ConvertToOC<T>(List<T> list)
        {
            var OC = new ObservableCollection<T>();

            foreach (var item in list)
            {
                OC.Add(item);
            }

            return OC;
        }
    }
}
