﻿using PO_Project.Infrastructure.Data;
using PO_Project.Models.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Repositories.Tags
{
    public class TagsRepository : ITagsRepository
    {
        private readonly DataContext _dataContext;
        public TagsRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<Tag> GetAll(Func<Tag, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Tag Insert(Tag entity)
        {
            throw new NotImplementedException();
        }

        public Tag Remove(Tag entity)
        {
            throw new NotImplementedException();
        }
    }
}
