﻿using PO_Project.Models.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Repositories.Tags
{
    public interface ITagsRepository : IRepositoryBase<Tag>
    {
    }
}
