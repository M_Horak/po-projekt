﻿using PO_Project.Models.Empoyees.Models;

namespace PO_Project.Infrastructure.Repositories.Employees
{
    public interface IEmployeesRepository : IRepositoryBase<Employee>
    {
    }
}
