﻿using PO_Project.Infrastructure.Data;
using PO_Project.Models.Empoyees.Models;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace PO_Project.Infrastructure.Repositories.Employees
{
    public class EmployeesRepository : IEmployeesRepository
    {
        private readonly DataContext _dataContext;
        public EmployeesRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public IList<Employee> GetAll(Func<Employee, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Employee Insert(Employee entity)
        {
            throw new NotImplementedException();
        }

        public Employee Remove(Employee entity)
        {
            throw new NotImplementedException();
        }
    }
}
