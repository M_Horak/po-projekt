﻿using PO_Project.Infrastructure.Data;
using PO_Project.Models.Subjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Repositories.Subjects
{
    public class SubjectsRepository : ISubjectsRepository
    {
        private readonly DataContext _dataContext;
        public SubjectsRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IList<Subject> GetAll(Func<Subject, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Subject Insert(Subject entity)
        {
            throw new NotImplementedException();
        }

        public Subject Remove(Subject entity)
        {
            throw new NotImplementedException();
        }
    }
}
