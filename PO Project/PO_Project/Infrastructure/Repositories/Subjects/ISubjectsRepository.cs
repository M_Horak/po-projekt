﻿using PO_Project.Models.Subjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PO_Project.Infrastructure.Repositories.Subjects
{
    public interface ISubjectsRepository : IRepositoryBase<Subject>
    {
    }
}
