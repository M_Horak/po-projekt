﻿using System;
using System.Collections.Generic;

namespace PO_Project.Infrastructure.Repositories
{
    public interface IRepositoryBase<T>
    {
        T Insert(T entity);
        T Remove(T entity);
        IList<T> GetAll(Func<T, bool> predicate);
    }
}
