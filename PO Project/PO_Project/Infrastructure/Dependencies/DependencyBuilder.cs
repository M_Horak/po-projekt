﻿using PO_Project.Infrastructure.Data;
using PO_Project.Infrastructure.Repositories.Employees;
using PO_Project.Infrastructure.Services.Employees;
using PO_Project.Infrastructure.Services.Logs;
using PO_Project.ViewModels.Config.Navigators;
using PO_Project.Visualisation.Binding;
using Ninject;
using System.Windows.Input;
using PO_Project.Infrastructure.Repositories.Subjects;
using PO_Project.Infrastructure.Repositories.Groups;
using PO_Project.Infrastructure.Repositories.Tags;
using PO_Project.Infrastructure.Services.Subjects;
using PO_Project.Infrastructure.Services.Groups;
using PO_Project.Infrastructure.Services.Tags;
using PO_Project.Infrastructure.Repositories.Fields;
using PO_Project.Infrastructure.Services.Fields;

namespace PO_Project.Infrastructure.Dependencies
{
    public class DependencyBuilder
    {
        private readonly IKernel _kernel;
        public DependencyBuilder(IKernel kernel)
        {
            _kernel = kernel;
        }

        public T Get<T>() => _kernel.Get<T>();

        public void Build()
        {
            BindRepositories();
            BindServices();
            BindUtilityModels();
            BindDbConnection();
        }
        private void BindRepositories()
        {
            _kernel.Bind<ISubjectsRepository>().To<SubjectsRepository>().InSingletonScope();
            _kernel.Bind<IEmployeesRepository>().To<EmployeesRepository>().InSingletonScope();
            _kernel.Bind<IGroupsRepository>().To<GroupsRepository>().InSingletonScope();
            _kernel.Bind<IFieldsRespository>().To<FieldsRepository>().InSingletonScope();
            _kernel.Bind<ITagsRepository>().To<TagsRepository>().InSingletonScope();
        }

        private void BindServices()
        {
            _kernel.Bind<ISubjectService>().To<SubjectService>().InTransientScope();
            _kernel.Bind<IEmployeeService>().To<EmployeeService>().InTransientScope();
            _kernel.Bind<IGroupService>().To<GroupService>().InTransientScope();
            _kernel.Bind<IFieldService>().To<FieldService>().InSingletonScope();
            _kernel.Bind<ITagService>().To<TagService>().InTransientScope();

            _kernel.Bind<ILoggingService>().To<LoggingService>().InTransientScope();
        }

        private void BindUtilityModels()
        {
            _kernel.Bind<ICommand>().To<RelayCommand>();
            _kernel.Bind<INavigator>().To<Navigator>();
            _kernel.Bind<DependencyBuilder>().ToSelf();
        }

        private void BindDbConnection()
        {
            /*var factory = new DataContextFactory();
            var context = factory.CreateDbContext(null);
            _kernel.Bind<DataDbContext>().ToConstant(context);*/
        }
    }
}
